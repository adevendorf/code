

function calculate() {
  var results = {};
  
  products.forEach(o => {
    results[o.name] = {score: 0};
  });

  for (var i = 0; i < choices.length; i++) {
    products.forEach(o => {
      if (o.values[i] == choices[i]) {
        results[o.name].score += 1;
      }
    })
  }

  for (r in results) {
    results[r].pct = Math.round((results[r].score / choices.length) * 100) + '%';
  }

  console.log(results);
}

calculate();
