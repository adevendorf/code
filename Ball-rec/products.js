export default {
  products: [
    {
      code: 'tp5', 
      name: 'TP5', 
      values: [1, 1, 1] 
    },
    {
      code: 'tp5x', 
      name: 'TP5x', 
      values: [0, 0, 0] 
    },
  ],
}