import DOM from '../../shared/dom';
import SFL from './modules/sfl';

let bucketed = false;
let _$ = window.$;

function bucket() {
  if (!bucketed) {
    bucketed = true;
    Adobe.bucket('eVar45', 'tm-save-for-later', 'default');
  }
}

(function() {	
  DOM.when('#cart-items-form', function(elem) {   

    // Assign a value to each submit/link since there are issues tracking clicks on submits
  	_$('#cart-items-form button[type="submit"]').each(function () {
  		const random = Math.random().toString(36).substring(7);
  		if (_$(this).attr('value') == 'Save for Later' ) {
  			_$(this).attr('data-tracking-id', 'btn_' + random);
  		}
  	});    
    _$('.not-loggedin-saveforlater-link').each(function () {
      const random = Math.random().toString(36).substring(7);
      if (_$(this).attr('title') == 'Save for Later') {
        _$(this).attr('data-tracking-id', 'btn_' + random);
      }
    });
    _$('.saveforlater-cart-row .item-user-actions .button-secondary').each(function () {
      const random = Math.random().toString(36).substring(7);
      _$(this).attr('data-tracking-id', 'btn_' + random);
    }); 

		// Adding to Saved Bag if NOT logged in
		// _$('.not-loggedin-saveforlater-link').on('click', function(e) {
		// 	if (_$(this).attr('data-tracking-id')) {
  //       e.preventDefault();

  //       SFL.trackSaveForLater(_$(this));

  //       _$(this).removeAttr('data-tracking-id');

  //       setTimeout(() => {
  //         _$(this).click();
  //       }, 200);
  //     }
		// });

		// Adding to Saved Bag if logged in
		_$('.item-user-actions button[value="Save for Later"]').on('click', function(e) {
			if (_$(this).attr('data-tracking-id')) {
				e.preventDefault();

				SFL.trackSaveForLater(_$(this));

				_$(this).removeAttr('data-tracking-id');

  			setTimeout(() => {
	  			_$(this).click();
	  		}, 200);
			}
		});


  	// Add back to Cart
  	_$('.saveforlater-cart-row  .item-user-actions .button-secondary').on('click', function(e) {
  		if (_$(this).attr('data-tracking-id')) {
  			e.preventDefault();

  			SFL.trackAddToCart(_$(this));

  			_$(this).removeAttr('data-tracking-id');

  			setTimeout(() => {
          console.log('CLICK', _$(this));
	  			_$(this).click();
	  		}, 200);
  		}  		
  	}); 
  });
})();