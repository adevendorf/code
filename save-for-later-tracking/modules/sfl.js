import Cookie from '../../../shared/cookies';
import Adobe from '../../../shared/adobe';

let _$ = window.$;

export default {
	cookieName: 'tm_sfl',

	getSku($el) {
		return $el.closest('.cart-row').data('productid');
	},

	getMasterProductId($el) {
		return $el.closest('.cart-row').data('masterproductid');
	},

	trackSaveForLater($el) {
		var productId = this.getSku($el);
		var masterProdId = this.getMasterProductId($el);
		var cookieVal = Cookie.get(this.cookieName);
		var sessionSFL = Cookie.get('tm_saved_for_later_session');
		var data = cookieVal ? cookieVal : {};
		var addSessionProp = false;

		if (!sessionSFL) {
			addSessionProp = true;
			Cookie.set('tm_saved_for_later_session', { saved: productId, master: masterProdId }, true);
		}

		var props = { 
			prop14: 'exp-sfl-saved-item'
		};

		if (addSessionProp) {
			props.prop15 = 'exp-sfl-new-session';
		}

		Adobe.track(props, 'eVar45=save-for-later-user', 'event47=1', [masterProdId || productId]);

		data[productId] = { 
			master: masterProdId, 
			saved: Date.parse(new Date())
		};

		Cookie.set(this.cookieName, data, false);
	},

	trackAddToCart($el) {
		var productId = this.getSku($el);
		var masterProdId = this.getMasterProductId($el);
		var now = Date.parse(new Date());
		var cookieVal = Cookie.get(this.cookieName);
		var data = cookieVal ? cookieVal : {};
		
		if (data[productId]) {
			if (!data[productId].master) {
				data[productId].master = masterProdId;
			}

			data[productId].added = Date.parse(new Date());	

			Cookie.set(this.cookieName, data, true);

			var props = { 
				prop14: 'exp-sfl-moved-back-to-cart'
			};

			Adobe.track(props, 'eVar45=save-for-later-user', null, [masterProdId || productId]);
		}	else {
			console.log('>> Product exists in cookie')
		}
	},

	trackCompleted(skus) {
		var storedOn;
		var now = Date.parse(new Date());
		var cookieVal = Cookie.get(this.cookieName);
		var data = cookieVal ? cookieVal : {};
		var products = [];
		for (let i = 0; i < skus.length; i++) {
			const sku = skus[i];

			if (data[sku] && data[sku].added) {
				if (data[sku].master) {
					products.push({ sku: sku, master: data[sku].master });
				} else {
					products.push({ sku: sku, master: sku });
				}
			}
		}

		for (let p = 0; p < products.length; p++) {
			var product = products[p];
			console.log('PRODUCT', product)
			var props = { 
				prop14: 'exp-sfl-purchased'
			};
			storedOn = data[product.sku].saved;
			console.log('STORED', storedOn)
			console.log('NOW', now)
			var diff = (now - storedOn) / 1000 / 60 / 60;

			console.log('DIFF', diff)
			var days = Math.round(diff / 24);
			delete data[product.sku]; 
			console.log('DAYS ', days, [product.master || product.sku])
			Adobe.track(props, 'eVar45=save-for-later-user', 'event46=' + days, [product.master || product.sku]);
		}

		Cookie.set(this.cookieName, data, false);
	},	
};
