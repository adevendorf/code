import DOM from '../../shared/dom';
import SFL from './modules/sfl';

let skus = [];
let _$ = window.$;

(function() {	
  DOM.when('.order-confirmation-details', function(elem) {   
  	console.log('start confirmation tracking');

  	const ordered = _$('#cart-table .item-details').each(function() {
  		const sku = _$(this).find('.sku .value').text();
  		skus.push(sku);
  	});

  	SFL.trackCompleted(skus);
  });
})();