(function(window, PLP) {

  new ScrollMagic.Scene({
    triggerElement: "#section-1-control",
    duration: $(window).height(),
  })
  .on("enter", function (e) {
    $('#section-1-control').addClass('active');

    PLP.activeClubType('m1');
    
    if (!PLP.enteredM1Control) {
      PLP.enteredM1Control = true;
      PLP.M1CONTROL.animateIntro();
    }      
  })
  .addTo(scrollMagicController);


  PLP.M1CONTROL = {
    breakPoint: 1200,
    TTRACK: null,
    ctx: null,
    draggableRangeX: {
      val: 0
    },
    animations: {},
    instance: {
      track1: null,
      track2: null
    },
    resetTimer: null,

    resetTracks: function() {

      clearTimeout(PLP.M1CONTROL.resetTimer);

      PLP.M1CONTROL.resetTimer = setTimeout(function() {
        try {
          $('#drag-track-1 .handle, #drag-track-2 .handle').hide();
          PLP.M1CONTROL.draggableRangeX.val = 0;

          setTimeout(function() {

            PLP.M1CONTROL.makeDraggable();

            TweenLite.set($("#drag-track-1"), {x:0});
            TweenLite.set($("#drag-track-2"), {y:0});

            
          }, 200);
        } catch(e) {}
      }, 100);      
    },

    animateIntro: function() {
      PLP.M1CONTROL.animations.tween1 = TweenLite.to(PLP.M1CONTROL.draggableRangeX, .5, {
        val: .85, 
        onUpdate: PLP.M1CONTROL.moveDragables, 
        ease: Linear.easeOut
      });

      setTimeout(function() {
        PLP.M1CONTROL.animations.tween2 = TweenLite.to(PLP.M1CONTROL.draggableRangeX, .5, {
          val:  0, 
          onUpdate: PLP.M1CONTROL.moveDragables, 
          ease: Linear.easeOut
        });
      }, 1100);    

    },

    makeDraggable: function() {
      $('#drag-track-1, #drag-track-2').show();
       $('#drag-track-1 .handle, #drag-track-2 .handle').show();

      PLP.M1CONTROL.instance.track1 = Draggable.create('#handle-1', {
        type: "x",
        bounds: "#drag-track-1",
        throwProps: true,
        lockAxis:true,
        allowNativeTouchScrolling: false,
        onDrag: function() {
          var pct = this.x / this.maxX;
          PLP.M1CONTROL.TTRACK.useXSliderPct(pct);
        }
      });

      PLP.M1CONTROL.instance.track2 = Draggable.create('#handle-2', {
        type: "y",
        bounds: "#drag-track-2",
        throwProps: true,
        lockAxis:true,
        allowNativeTouchScrolling: false,
        onDrag: function() {
          var pct = this.y / this.maxY;
          PLP.M1CONTROL.TTRACK.useYSliderPct(pct);
        }
      });
    },

    moveDragables: function() {
      PLP.M1CONTROL.TTRACK.useXSliderPct(PLP.M1CONTROL.draggableRangeX.val);
      PLP.M1CONTROL.TTRACK.useYSliderPct(PLP.M1CONTROL.draggableRangeX.val);
    },


    drawBallPath: function() {
      var ctx = PLP.M1CONTROL.ctx;

      //shadow
      ctx.beginPath();
      ctx.fillStyle = '#333';
      ctx.shadowBlur = 0;
      ctx.moveTo((ctx.canvas.clientWidth / 2) - 25, ctx.canvas.clientHeight);
      ctx.quadraticCurveTo(
        PLP.M1CONTROL.cp2X,
        PLP.M1CONTROL.cp2Y,
        PLP.M1CONTROL.ep1X-3,
        PLP.M1CONTROL.ep1Y
      );
      ctx.lineTo(PLP.M1CONTROL.ep1X+3, PLP.M1CONTROL.ep1Y);
      ctx.quadraticCurveTo(
        PLP.M1CONTROL.cp2X,
        PLP.M1CONTROL.cp2Y,
        (ctx.canvas.clientWidth / 2) + 25,
        ctx.canvas.clientHeight
      );
      ctx.lineTo((ctx.canvas.clientWidth / 2) - 25, ctx.canvas.clientHeight);
      ctx.fill();
      ctx.closePath();

      //ball
      ctx.beginPath();
      ctx.fillStyle = '#cbd500';
      ctx.moveTo((ctx.canvas.clientWidth / 2) - 25, ctx.canvas.clientHeight);
      ctx.quadraticCurveTo(
        PLP.M1CONTROL.cp1X,
        PLP.M1CONTROL.cp1Y,
        PLP.M1CONTROL.ep1X-3,
        PLP.M1CONTROL.ep1Y
      );
      ctx.lineTo(PLP.M1CONTROL.ep1X+3, PLP.M1CONTROL.ep1Y);
      ctx.quadraticCurveTo(
        PLP.M1CONTROL.cp1X,
        PLP.M1CONTROL.cp1Y,
        (ctx.canvas.clientWidth / 2) + 25,
        ctx.canvas.clientHeight
      );
      ctx.lineTo((ctx.canvas.clientWidth / 2) - 25, ctx.canvas.clientHeight);
      ctx.shadowBlur = 30;
      ctx.shadowColor = "#cbd500";
      ctx.fill();
      ctx.closePath();

      //outline
      ctx.beginPath();
      ctx.fillStyle = 'transparent';
      ctx.strokeStyle = '#c3cd00';
      ctx.moveTo((ctx.canvas.clientWidth / 2) - 25, ctx.canvas.clientHeight);
      ctx.quadraticCurveTo(
        PLP.M1CONTROL.cp1X,
        PLP.M1CONTROL.cp1Y,
        PLP.M1CONTROL.ep1X-3,
        PLP.M1CONTROL.ep1Y
      );
      ctx.lineTo(PLP.M1CONTROL.ep1X+3, PLP.M1CONTROL.ep1Y);
      ctx.quadraticCurveTo(
        PLP.M1CONTROL.cp1X,
        PLP.M1CONTROL.cp1Y,
        (ctx.canvas.clientWidth / 2) + 25,
        ctx.canvas.clientHeight
      );
      ctx.stroke();
      ctx.closePath();
    },

    drawCourse: function() {
      var ctx = PLP.M1CONTROL.ctx;

      ctx.beginPath();
      ctx.moveTo(0, ctx.canvas.clientHeight);
      ctx.strokeStyle = '#067c00';
      ctx.lineTo((ctx.canvas.clientWidth / 2) - (ctx.canvas.clientWidth * .07), ctx.canvas.clientHeight * .5);
      ctx.stroke();

      ctx.beginPath();
      ctx.moveTo(ctx.canvas.clientWidth , ctx.canvas.clientHeight);
      ctx.strokeStyle = '#067c00';
      ctx.lineTo((ctx.canvas.clientWidth / 2) + (ctx.canvas.clientWidth * .07), ctx.canvas.clientHeight * .5);
      ctx.stroke();

      ctx.beginPath();
      ctx.moveTo((ctx.canvas.clientWidth / 2) - (ctx.canvas.clientWidth * .07), ctx.canvas.clientHeight * .5);
      ctx.strokeStyle = '#004100';
      ctx.lineTo((ctx.canvas.clientWidth / 2) + (ctx.canvas.clientWidth * .07), ctx.canvas.clientHeight * .5);
      ctx.stroke();
    },

    render: function() {
      var ctx = PLP.M1CONTROL.ctx;

      ctx.clearRect(0, 0, ctx.canvas.clientWidth, ctx.canvas.clientHeight);
      PLP.M1CONTROL.drawBallPath();

      setTimeout(PLP.M1CONTROL.render, 100);
    },

    updateDrawFadeTrack: function(val) {
      var ctx = PLP.M1CONTROL.ctx;
      val = val - 16;

      PLP.M1CONTROL.cp1X = (ctx.canvas.clientWidth / 2) + (((parseInt(val) * 2.5)) * -5);
      PLP.M1CONTROL.cp2X = (ctx.canvas.clientWidth / 2) + (((parseInt(val) * 2.5)) * -3);
    },

    updateFrontBackTrack: function(val) {
      var ctx = PLP.M1CONTROL.ctx;
      val = (val - 16) * -1;

      PLP.M1CONTROL.cp1Y = (ctx.canvas.clientHeight * .06) + (parseInt(val) * -4);
    },

    sizeBallPathDrawing: function() {
      var ctx = PLP.M1CONTROL.ctx;

      ctx.canvas.width  = document.getElementById('ballpath-wrapper').offsetWidth;
      ctx.canvas.height = document.getElementById('ballpath-wrapper').offsetHeight;

      PLP.M1CONTROL.cp1X = ctx.canvas.clientWidth / 2;
      PLP.M1CONTROL.cp1Y = ctx.canvas.clientHeight * .04;

      PLP.M1CONTROL.cp2X = ctx.canvas.clientWidth / 2;
      PLP.M1CONTROL.cp2Y = ctx.canvas.clientHeight * .7;

      PLP.M1CONTROL.ep1X = ctx.canvas.clientWidth / 2;
      PLP.M1CONTROL.ep1Y = ctx.canvas.clientHeight * .6;
    },

    killAnimation: function() {
      if (PLP.M1CONTROL.animations.tween1) PLP.M1CONTROL.animations.tween1.kill();
      if (PLP.M1CONTROL.animations.tween2) PLP.M1CONTROL.animations.tween2.kill();
    }
  }; 
  
  var c=document.getElementById("ballpath");
  PLP.M1CONTROL.ctx = c.getContext("2d");   
  PLP.M1CONTROL.sizeBallPathDrawing();

  var TTrack = function() {
  	var _this = this;
  	this.FRONT = $('#dynamic-fronttrack');
  	this.REAR = $('#dynamic-reartrack');
  	this.TRACKER = $('#mouse-tracker');
  	this.ENABLED = true;
  	this.X = $('#x');
  	this.Y = $('#y');
  	this.xFrame = null;
  	this.yFrame = null;
  	this.totalFrames = 31;
  	this.aniTimer = null;
  	this.animatedFrames = [15,16,17,18,19,20,21,22,23,24,25,25,26,26,27,27,28,28,28,29,29,29,30,30,30,30,30,29,29,28,28,27,26,25,24,23,22,21,20,19,18,17,16,15]
  	this.animatedFrame = 0;
    this.topTrack = {
    };
    this.bottomTrack = {
    };

    this.setSizes = function() {
      this.topTrack = {
        width: 259, 
        height: 64,
      };

      this.bottomTrack = {
       width: 73, //($(window).width() >= PLP.M1CONTROL.breakPoint ? 1 : 2),
        height: 230 //($(window).width() >= PLP.M1CONTROL.breakPoint ? 1 : 2)
      };
    }

    this.reset = function() {
      PLP.M1CONTROL.TTRACK.setSizes();
      PLP.M1CONTROL.sizeBallPathDrawing();
      PLP.M1CONTROL.draggableRangeX.val = 0;
      PLP.M1CONTROL.TTRACK.startingPosition();
      PLP.M1CONTROL.TTRACK.useXSliderPct(PLP.M1CONTROL.draggableRangeX.val);
      PLP.M1CONTROL.TTRACK.useYSliderPct(PLP.M1CONTROL.draggableRangeX.val);
    };

  	this.endTracking = function() {

      if (PLP.M1CONTROL.resized) return;

  		_this.ENABLED = false;
     
      PLP.M1CONTROL.animations.tween2 = TweenLite.to(PLP.M1CONTROL.draggableRangeX, .75, {
        val:  0, 
        onUpdate: PLP.M1CONTROL.moveDragables, 
        ease: Linear.easeOut
      });

      $(this.TRACKER).off('mousemove', this.getPositionPct);
      PLP.M1CONTROL.makeDraggable();
  	};

  	this.drawFrameTop = function(frame, pct) {
  		PLP.M1CONTROL.updateDrawFadeTrack(frame);

  		var row =  Math.floor(frame / 4);
  		var col = (frame) % 4;

  		var xPos = (col * _this.topTrack.width) * -1
  		var yPos = (row * _this.topTrack.height) * -1;

  		_this.FRONT.css({
  			'background-position-y': (yPos) + 'px',
  			'background-position-x': (xPos) + 'px'
  		});

       console.log(col, row, xPos, yPos);
  	};

  	this.drawFrameBottom = function(frame, pct) {
  		PLP.M1CONTROL.updateFrontBackTrack(frame);

  		var row =  Math.floor(frame / 8);
  		var col = (frame) % 8;

  		var xPos = (col * _this.bottomTrack.width) * -1
  		var yPos = (row * _this.bottomTrack.height) * -1;

  		_this.REAR.css({
  			'background-position-y': yPos + 'px',
  			'background-position-x': xPos + 'px'
  		});
  	};

    this.useXSliderPct = function(xPercent) {
      var xFrame = _this.totalFrames - Math.floor(_this.totalFrames * xPercent);

      _this.drawFrameTop(xFrame, xPercent);
    }

    this.useYSliderPct = function(yPercent) {
      var yFrame = _this.totalFrames - Math.floor(_this.totalFrames * yPercent);

      _this.drawFrameBottom(yFrame, yPercent);
    }

  	this.getPositionPct = function(e) {
  		e.preventDefault();

      PLP.M1CONTROL.killAnimation();

  		if (!_this.ENABLED) return;

  		var offset = $(this).offset();
  		var height = _this.TRACKER.height();
  		var width = _this.TRACKER.width();
  		var x = (e.pageX - offset.left);
  		var y = (e.pageY - offset.top);

  		_this.xPct = (x/width);
  		_this.yPct = (y/height);

  		if (_this.xPct < 0) _this.xPct = 0;
  		if (_this.xPct > 1) _this.xPct = 1;

  		if (_this.yPct < 0) _this.yPct = 0;
  		if (_this.yPct > 1) _this.yPct = 1;


  		var xFrame = _this.totalFrames - Math.floor(_this.totalFrames * _this.xPct);
  		var yFrame = _this.totalFrames - Math.floor(_this.totalFrames * _this.yPct);

  		_this.drawFrameTop(xFrame, _this.xPct);
  		_this.drawFrameBottom(yFrame, _this.yPct);
  	};


  	this.centerPosition = function() {
  		_this.drawFrameTop(Math.round(_this.totalFrames / 2));
  		_this.drawFrameBottom(Math.round(_this.totalFrames / 2));
  	};


    this.startingPosition = function() {
      _this.drawFrameTop(30);
      _this.drawFrameBottom(30);
    };  

  	this.animate = function() {
  		_this.drawFrameTop(_this.animatedFrames[_this.animatedFrame]);
  		_this.drawFrameBottom(_this.animatedFrames[_this.animatedFrame]);

  		if (_this.animatedFrame == _this.animatedFrames.length) return;

  		_this.aniTimer = setTimeout(function() {
  			var self = _this;
  			self.animatedFrame++;
  			self.animate();
  		}, 16);
  	};

    this.setSizes();
  	this.startingPosition();
  };

  PLP.M1CONTROL.TTRACK = new TTrack();

  $(function() {  	
    PLP.M1CONTROL.render();

    if (window.isMobileDevice()) {  
      PLP.M1CONTROL.makeDraggable();
    } else {
      $(PLP.M1CONTROL.TTRACK.TRACKER).on('click', PLP.M1CONTROL.TTRACK.endTracking);
      $(PLP.M1CONTROL.TTRACK.TRACKER).on('mousemove', PLP.M1CONTROL.TTRACK.getPositionPct);

      setTimeout(function() {
        $(window).on('resize', PLP.M1CONTROL.TTRACK.reset);
        if (!PLP.M1CONTROL.resized) {
          PLP.M1CONTROL.resized = true;
          $(window).on('resize', PLP.M1CONTROL.TTRACK.endTracking);
        }
      }, 200);
    }  

    

  });

})(window, window.PLP);
