function Metalwood(props) {
    return (
        <section id="s1">
            <div  className="gchart-slide">
                <h2>What is your shortest metalwood?</h2>
                <div class="cta">
                <button onClick={ () => props.choicehandler('metalwood', 'c')}>5W</button>
                <button onClick={ () => props.choicehandler('metalwood', 'b')}>3HL</button>
                <button onClick={ () => props.choicehandler('metalwood', 'a')}>3W</button>
                </div>


                <button className="back-button" onClick={ () => props.gotohandler(0)}>Back</button>
            </div>
        </section>
    )
}

export default Metalwood;