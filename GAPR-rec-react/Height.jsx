function Height(props) {
    return (
        <section id="s3">
         <div  className="gchart-slide">
            <h2>What is your ideal ball flight?</h2>
            <div class="cta">
            <button  onClick={ () => props.choicehandler('flight', 'LO') }>Low</button>
            <button  onClick={ () => props.choicehandler('flight', 'MID') }>Average</button>
            <button  onClick={ () => props.choicehandler('flight', 'HI') }>High</button>

            </div>

            <button className="back-button" onClick={ () => props.gotohandler(2)}>Back</button>
            </div>
        </section>
    )
}

export default Height;