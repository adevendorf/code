function Iron(props) {
    return (
        <section id="s2">
        <div  className="gchart-slide">
             <h2>What is your longest playable iron?</h2>
             <div class="cta">
                <button onClick={ () => props.choicehandler('iron', 'd')}>6</button>
                <button onClick={ () => props.choicehandler('iron', 'c')}>5</button>
                <button onClick={ () => props.choicehandler('iron', 'b')}>4</button>
                <button onClick={ () => props.choicehandler('iron', 'a')}>3</button>

            </div>

            <button className="back-button" onClick={ () => props.gotohandler(1)}>Back</button>
            </div>
        </section>
    )
}

export default Iron;