import ReactDOM from 'react-dom';
import App from './App.jsx';


(function() {

	function render() {
		ReactDOM.render(
			<App/>, 
			document.getElementById('gapr-recommender')
		);
	}

	render();
})();