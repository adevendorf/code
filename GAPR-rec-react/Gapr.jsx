function getProduct(productList, flight, club) {
    if (productList[flight] && productList[flight][club]) {
        return {
            name: 'GAPR ' + (flight).toUpperCase(),
            number: club,
            loft: productList[flight][club].loft,
        };
    } 
        
    if (flight == 'LO') return getProduct(productList, 'MID', club);

    // FOR ERROR LOGGING PURPOSES
    return {
        name: 'x',
        number: 'x',
        loft: 'x',
    };
}
function getImage(club) {
    return 'gapr-' + club + '.png';
}

function Gapr(props) {
    let product = getProduct(props.products, props.flight, props.club);

    return (
        <div class="gapr-item">
            <img src={'images/' + getImage(props.flight)} alt={product.name } />
            <h3>{product.name }</h3> 
            {product.number } - <span dangerouslySetInnerHTML={{ __html: product.loft}} />
        </div>  
    );
}

export default Gapr;