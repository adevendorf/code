import Gapr from './Gapr.jsx';

function getOptions(choices, collection) {
    if (!choices.iron || !choices.metalwood) return null;

    var mwOptions = collection.filter(obj => {
      return obj.mw === choices.metalwood;
    });

    var clubs = mwOptions.filter(obj => {
      return obj.iron == choices.iron;
    });

    if (clubs.length > 0) return clubs[0];

    return [];
}

function Results(props) {
    var options = getOptions(props.choices, props.collection);
    var recommendations = options.rec;

    console.log('RECC', recommendations);

    return (
        <section id="s4">
            <div class="gchart-slide">
                <h2>Your GAPR Recommendation</h2>

                <p>Loft Gap: <span dangerouslySetInnerHTML={{ __html: options.loftgap}} /></p>
                <p>Your Distance Gap: {options.distancegap}</p>

                <ul className="gapr-results">
                    {recommendations.map((club) => { 
                        return (
                            <li key={'club-' + club}>
                                <Gapr options={options} flight={props.choices.flight} club={club} products={props.products} />
                            </li>
                        );
                    })}
                </ul>

            
            </div>

            <button className="back-button" onClick={ () => props.gotohandler(3)}>Back</button>
            <button onClick={ () => props.gotohandler(0)}>Reset</button>
        </section>
    )
}
/*

*/

export default Results;