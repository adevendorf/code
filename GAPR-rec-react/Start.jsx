function Start(props) {
    return (
        <div id="start" className="gchart-slide">
            <h2>ADJUSTABLE & FORGIVING</h2>
            <p>FIND THE GAP IN YOUR BAG AND WE'LL HELP YOU DISCOVER THE GAPR THAT WILL COMPLETE YOUR GAME.</p>
            <div className="cta">
                <button class="uikit-btn uikit-btn-thin uikit-btn-outline" onClick={ () => props.gotohandler(1)}>Find Your Gap</button>
            </div>

            <img className="large" src="images/Intro@2x.jpg" alt="Find your GAPR" />
        </div>
    );
}

export default Start;