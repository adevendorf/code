import Start from './Start.jsx';
import Metalwood from './Metalwood.jsx';
import Iron from './Iron.jsx';
import Height from './Height.jsx';
import Results from './Results.jsx';


class App extends React.Component {
	constructor(props) {
		super(props);
		
        this.state = {
            step: 0,
        
            choices: {
                iron: null,
                metalwood: null,
                flight: null,
            },
    
            collection: [
    
                { mw: 'a', iron: 'a', rec: [3], loftgap: '3&deg;', distancegap:'10-15 yards'},
                { mw: 'a', iron: 'b', rec: [3], loftgap: '5.5&deg;', distancegap:'15-20 yards'},
                { mw: 'a', iron: 'c', rec: [4,3], loftgap: '8&deg;', distancegap:'15-25 yards'},
                { mw: 'a', iron: 'd', rec: [5,4,3], loftgap: '11.5&deg;', distancegap:'20-30 yards'},
        
        
                { mw: 'b', iron: 'a', rec: [3], loftgap: '1.5&deg;', distancegap:'10-15 yards'},
                { mw: 'b', iron: 'b', rec: [3], loftgap: '4&deg;', distancegap:'15-20 yards'},
                { mw: 'b', iron: 'c', rec: [4,3], loftgap: '6.5&deg;', distancegap:'15-25 yards'},
                { mw: 'b', iron: 'd', rec: [5,4,3], loftgap: '10&deg;', distancegap:'20-30 yards'},
        
        
                { mw: 'c', iron: 'a', rec: [3], loftgap: 'SAME', distancegap:'10-15 yards'},
                { mw: 'c', iron: 'b', rec: [4], loftgap: '2.5&deg;', distancegap:'10-15 yards'},
                { mw: 'c', iron: 'c', rec: [3], loftgap: '5&deg;', distancegap:'15-20yards'},
                { mw: 'c', iron: 'd', rec: [4,3], loftgap: '8.5&deg;', distancegap:'15-25 yards'},
            
            ],
    
            products: {
                HI: {
                    6: {loft: '28&deg;'},
                    5: {loft: '25&deg;'},
                    4: {loft: '22&deg;'},
                    3: {loft: '19&deg;'},
                },
                MID: {
                    5: {loft: '24&deg;'},
                    4: {loft: '21&deg;'},
                    3: {loft: '18&deg;'},
                },
                LO: {
                    4: {loft: '22&deg;'},
                    3: {loft: '19&deg;'},
                    2: {loft: '17&deg;'},
                }
            },
        };

        this.goToHandler = this.goToHandler.bind(this);
        this.choiceHandler = this.choiceHandler.bind(this);
    }

    goToHandler(i) {
        this.setState({
            step: i
        });
    }

    choiceHandler(key, value) {
        if (key && value) {
            var newChoices = this.state.choices;
            newChoices[key] = value; 
            
            this.setState({
                choices: newChoices
            });

            var newStep = this.state.step + 1;

            this.setState({
                step: newStep
            });
        }
    }



    render() {
        return (
            <section id="main-app">
                {(() => {
                    switch(this.state.step) {
                        case 0:
                            return <Start gotohandler={this.goToHandler} />;
                        case 1:
                            return <Metalwood gotohandler={this.goToHandler} choicehandler={this.choiceHandler} />;
                        case 2:
                            return <Iron gotohandler={this.goToHandler} choicehandler={this.choiceHandler} />;
                        case 3:
                            return <Height gotohandler={this.goToHandler} choicehandler={this.choiceHandler} />;    
                        case 4:
                            return <Results gotohandler={this.goToHandler} collection={this.state.collection} choices={this.state.choices} products={this.state.products} />;    
                        default:
                            return null;
                    }
                })()}
            </section>  
        );
    }
}

export default App;