import Vue from 'vue';
import App from './App.vue';

window.bus = new Vue();

const app = new Vue({	
	el: '#gapr-recommender',
	template: '<App/>',
	components: { App },	
});
